exports.getUserByName=function(input,cb){
	sql.connect(config.serverConnect).then(err=>{
		const request = new sql.Request()
		request.input("account",input.username)
		request.execute("sp_Search_Account",(err,result)=>{
			if(err || !result) cb(err)
			else{
				cb(null,result.recordset)
			}
		})
	})
}


exports.register=function(input,cb){
	sql.connect(config.serverConnect).then(err=>{
		const request = new sql.Request()
		request.input("account",input.username)
		request.input("password",input.password)
		request.execute("sp_Register_Account",(err,result)=>{
			if(err || !result) cb(err)
			else{
				cb(null,result.rowsAffected)
			}
		})
	})
}

exports.updateOTP=function(input,cb){
	sql.connect(config.serverConnect).then(err=>{
		const request = new sql.Request()
		request.input("account",input.username)
		request.input("OTP",input.otp)
		request.execute("sp_Update_OTP",(err,result)=>{
			if(err || !result) cb(err)
			else{
				cb(null,result.recordset)
			}
		})
	})
}