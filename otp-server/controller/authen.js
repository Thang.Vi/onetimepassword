exports.login=function(req,res){
	dao.user.getUserByName({
		username: req.body.username,
		password: req.body.password,
		otp: req.body.otp
	},(err,user)=>{
		if(err || !user) res.end("not found account")
		else{
			let username = req.body.username.toString()
			let password = req.body.password.toString()

			let passwordEncryptString = (config.passwordKey + password).toString()
			var myBuffer = [];
			var buffer = new Buffer(passwordEncryptString, 'utf16le');
			let passwordEncrypt = buffer.toString('base64')

			if(user[0] && passwordEncrypt == user[0].Password) {

				// Kiểm tra Có truyền OTP Lên hay không
				if(req.body.otp && req.body.otp.length > 0) {
					console.log(req.body.otp)
					console.log(user[0].OTP)
					if(req.body.otp == user[0].OTP)
						res.json({"token": jwt.sign({Account: user[0].Account, Password: user[0].Password}, 'TatuSecretKey'),
						"account": {"AccountID": user[0].AccountID, "Account": user[0].Account}});
					else {
						res.end("OTP Incorrect")
					}

				} else {
					var NewOTP = '';
				    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
				    for ( var i = 0; i < 8; i++ ) {
				        NewOTP += characters.charAt(Math.floor(Math.random() * characters.length));
				    }
				    dao.user.updateOTP({
						username: username,
						otp: NewOTP
					},(err,userUpdate)=>{
						services.notification.send({
							to: req.body.username,
							subject:"OTP",
							text:`Mã OTP của bạn là ${NewOTP}`
						})
					})

					res.json({"account": {"AccountID": user[0].AccountID, "Account": user[0].Account, "OTP": NewOTP}});
				}

				
			}else if(user[0] && passwordEncrypt != user[0].Password){
				res.end("Password Incorrect")
			} else(
				res.end("Account does not exist")
			)
		}
	})
}

exports.register=function(req,res){
	let username = req.body.username.toString()
	let password = req.body.password.toString()

	let passwordEncryptString = (config.passwordKey + password).toString()
	var myBuffer = [];
	var buffer = new Buffer(passwordEncryptString, 'utf16le');
	let passwordEncrypt = buffer.toString('base64')

	dao.user.getUserByName({
		username: req.body.username
	}, (err,user)=>{
		if(user && user.length > 0) {
			res.end("User has exist!")
		}
		else{
			dao.user.register({
				username: username,
				password: passwordEncrypt
			},(err,user)=>{
				if(err || !user) res.end("register error")
				else{
					res.end("Register Success!")
				}
			})
		}
	})
}