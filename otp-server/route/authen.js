let express=require("express")
let route=express.Router()
let controller=require("../controller")
let middleware=require("../middleware")
let bodyParser=require("body-parser")

route.post("/login",controller.authen.login)
route.post("/register",controller.authen.register)

module.exports=route