let express=require("express");
let app=express();
let init=require("./init")
let middleware=require("./middleware")
let bodyParser=require("body-parser")

/*INIT*/
init.global({
	async:require("async"),
	_ : require("lodash"),
	config:require("config"),
	sql:require("mssql"),
	dao:require("./database/DAO"),
	services:require("./shared/services"),
	jwt:require('jsonwebtoken'),
	multer: require("multer"),
	fs: require("fs")
})


/*API*/
app.use("/*",
	middleware.corsFilter,
	middleware.apiResponse,
	bodyParser.urlencoded({extend:false}),
	bodyParser.json()
)

app.use("/authen",require("./route").authen)

app.listen(5000,function(){
	console.log("listen on 5000....")
})
