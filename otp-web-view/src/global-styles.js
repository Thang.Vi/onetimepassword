import { createGlobalStyle } from 'styled-components';
import 'bootstrap/dist/css/bootstrap.css';

const GlobalStyle = createGlobalStyle`
  html,
  body {
    height: 100%;
    width: 100%;
    font-size: 14px;
    line-height: 1.5;
    overflow-x: hidden;
  }

  html {
    font-size: 14px;
  }

  body {
    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  body.fontLoaded {
    font-family: 'Montserrat', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  #app {
    background-color: #fafafa;
    min-height: 100%;
    min-width: 100%;
  }

  p,
  label {
    line-height: 1.5em;
  }
  .mb-margin {
    @media(max-width: 767px) {
      margin-bottom: 1rem;
    }
  }
  
  .fw-100 {
    width: 100%;
  }

  .d-flex {
    display: flex;
  }

  .d-inline-flex {
    display: inline-flex;
  }

  .box-shadow {
    box-shadow: 1px 2px 5px 1px rgba(206,206,206,0.5);
  }

  .cursor-pointer{
    cursor: pointer;
  }

  @media(min-width: 1600px) {
    .container {
      max-width: 1540px !important;
    }
  }
  @media(min-width: 350px) {
    .visiable-xs {
      display: block;
    }
    .hidden-xs {
      display: none;
    }
  }
  @media(min-width: 768px) {
    .visiable-xs {
      display: none;
    }
    .hidden-xs {
      display: block;
    }
  }
`;

export default GlobalStyle;
