import styled from 'styled-components'

const Input = styled.input`
  margin: 10px 0 5px;
  border: 1px solid #e6e6e6;
  padding: 7px 15px;
  box-sizing: border-box;
  width: 100%;
  font-size: 12px;
  &:focus, &:hover {
  	outline: none;
  	box-shadow: none;
  }
`;

export default Input