import styled from 'styled-components'

const StyleLogo = styled.div`
	text-align: center;
	width: 100%;
	color: red;
	font-size: 40px;
	margin-bottom: 30px;
`;

export default StyleLogo