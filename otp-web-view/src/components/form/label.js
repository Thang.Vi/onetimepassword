import styled from 'styled-components'

const Label = styled.label`
  color: #000;
  text-transform: uppercase;
  font-size: 16px;
  font-weight: 600;
  margin-top:  15px
`;

export default Label