import styled from 'styled-components'
import theme from "../theme"

const WrapperFormLogin = styled.div`
	padding: 30px 25px;
	background-color: ${theme.colors.default};
	border-radius: 20px;
	max-width: 420px;
	text-align: left;
	width: 100%;
	box-shadow: 1px 2px 5px 1px rgba(206,206,206,0.5);
`;

export default WrapperFormLogin