import styled from 'styled-components'
import theme from "../theme";

const Button = styled.button`
    transition: all .3s ease;
    background: ${props => theme.colors[Object.keys(props).find(p => theme.colors[p])] || theme.colors.primary};
    text-transform: uppercase;
    font-weight: 600;
    color: ${theme.colors.default};
    margin: 1px;
    border: none;
    border-radius: 999px;
    cursor: pointer;
    width: 150px;
    margin: 0 auto;
    display: block;
    margin-top: 20px;
    letter-spacing: 3px;
    font-size: 14px;
    p {
        padding: 10px 25px;
        color: #fff;
    }
    > a {
        padding: 10px 25px;
        color: #fff;
        display: block;
        text-decoration: none;
    }
    &:hover {
        opacity: .7;
    }
    &:focus {
        outline: none;
        box-shadow: none;
    }
`;

export default Button