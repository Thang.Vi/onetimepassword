import styled from 'styled-components'
import theme from "../theme"

const ErrorText = styled.span`
  font-size: 12px;
  color: ${theme.forms.errorColor};
`;

export default ErrorText