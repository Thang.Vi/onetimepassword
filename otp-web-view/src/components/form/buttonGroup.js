import styled from 'styled-components'

const formGroup = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: space-bettween;
  width: 100%;
  margin-top: 10px;
`;

export default formGroup