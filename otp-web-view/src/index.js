import React from 'react';
import {
    render
} from 'react-dom'
import GlobalStyle from './global-styles';
import Root from './routes'

import registerServiceWorker from './registerServiceWorker'


render( <
    Root /> ,
    document.getElementById('root')
);
registerServiceWorker();