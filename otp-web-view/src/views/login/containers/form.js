import React from 'react';
import {Formik} from 'formik';
import { Link } from "react-router-dom";
import Button from '../../../components/form/button';
import FormGroup from '../../../components/form/formGroup';
import Input from '../../../components/form/input';
import Label from '../../../components/form/label';
import StyleLogo from '../../../components/form/styleLogo';
import WrapperFormLogin from '../../../components/form/wrapperFormLogin';
import ErrorText from "../../../components/form/error";
import ButtonGroup from "../../../components/form/buttonGroup";

const LoginForm = props => (
  <Formik
    initialValues={{username: '', password: ''}}
    {...props}
  >
    {({handleChange, handleBlur, values, handleSubmit, isSubmitting, errors, touched}) => (
      <WrapperFormLogin>
        <StyleLogo>
          OTP
        </StyleLogo>
        <FormGroup>
          {errors.genericError &&
            <ErrorText>{errors.genericError}</ErrorText>
          }
          <Label htmlFor="username">Email</Label>
          <Input
            name='username'
            id='username'
            placeholder={'Username'}
            onChange={handleChange('username')}
            onBlur={handleBlur('username')}
            error={errors.username}
            value={values.username}
            autocomplete='off'
          />
          {props.err.user ? <ErrorText>{props.err.user}</ErrorText> : ''}
          <Label htmlFor="password">Password</Label>
          <Input
            id='password'
            name='password'
            type={'password'}
            placeholder={'Password'}
            onChange={handleChange('password')}
            onBlur={handleBlur('password')}
            error={errors.password}
            value={values.password}
            autocomplete='off'
          />
          {props.err.pass ? <ErrorText>{props.err.pass}</ErrorText> : ''}

          { props.auth && props.auth.logged ? 
            <>
              <Label htmlFor="password">OTP</Label>
              <Input
                id='otp'
                name='otp'
                type={'text'}
                placeholder={'OTP'}
                onChange={handleChange('otp')}
                onBlur={handleBlur('otp')}
                error={errors.otp}
                value={values.otp}
                autocomplete='off'
              />
              {props.err.OTP ? <ErrorText>{props.err.OTP}</ErrorText> : ''}
            </>
            : ''
          }
          <ButtonGroup>
            <Button primary onClick={handleSubmit} type="submit" ><p>Login</p></Button>
            <Button secondary type="button" ><Link to={`/register`}>Register</Link></Button>
          </ButtonGroup>
        </FormGroup>
      </WrapperFormLogin>
    )}
  </Formik>
);

export default LoginForm;
