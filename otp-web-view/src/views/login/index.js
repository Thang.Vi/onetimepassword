import React, { useEffect } from 'react'

import useLogin from '../../state/auth/hooks/useLogin'

import Container from './containers/container'
import Form from './containers/form'
import Spinner from '../../components/spinner'

const Login = (props) => {
  const { from } = props.location.state || {from: {pathname: "/home"}};
  const [auth, setLogin, isLoading, err, token] = useLogin();

  useEffect(() => {
    if (token && token.length > 0) {
      props.history.push(from);
    }
  }, [token]);

  return (
    <Container>
      <Spinner show={isLoading} />
      <Form onSubmit={(values, actions) => setLogin({values, actions})} err={err} auth={auth}/>
    </Container>
  )
};

export default Login
