import React from 'react';
import {Formik} from 'formik';
import { Link } from "react-router-dom";
import Button from '../../../components/form/button';
import FormGroup from '../../../components/form/formGroup';
import Input from '../../../components/form/input';
import Label from '../../../components/form/label';
import StyleLogo from '../../../components/form/styleLogo';
import WrapperFormLogin from '../../../components/form/wrapperFormLogin';
import TextRed from "../../../components/form/error";
import ButtonGroup from "../../../components/form/buttonGroup";

const LoginForm = props => (
  <Formik
    initialValues={{username: '', password: ''}}
    {...props}
  >
    {({handleChange, handleBlur, values, handleSubmit, isSubmitting, errors, touched}) => (
      <WrapperFormLogin>
        <StyleLogo>
          OTP
        </StyleLogo>
        <FormGroup>
          {errors.genericError &&
            <TextRed>{errors.genericError}</TextRed>
          }
          <Label htmlFor="username">Email <TextRed>*</TextRed></Label>
          <Input
            name='username'
            id='username'
            placeholder={'Username'}
            onChange={handleChange('username')}
            onBlur={handleBlur('username')}
            error={errors.username}
            value={values.username}
            autocomplete='off'
          />
          {props.err.user ? <TextRed>{props.err.user}</TextRed> : ''}
          <Label htmlFor="password">Password <TextRed>*</TextRed></Label>
          <Input
            id='password'
            name='password'
            type={'password'}
            placeholder={'Password'}
            onChange={handleChange('password')}
            onBlur={handleBlur('password')}
            error={errors.password}
            value={values.password}
            autocomplete='off'
          />
          {props.err.pass ? <TextRed>{props.err.pass}</TextRed> : ''}
          <Label htmlFor="password">Confirm Password <TextRed>*</TextRed></Label>
          <Input
            id='passwordConfirm'
            name='passwordConfirm'
            type={'password'}
            placeholder={'Confirm password'}
            onChange={handleChange('passwordConfirm')}
            onBlur={handleBlur('passwordConfirm')}
            error={errors.passwordConfirm}
            value={values.passwordConfirm}
            autocomplete='off'
          />
          {props.err.passConfirm ? <TextRed>{props.err.passConfirm}</TextRed> : ''}
          <ButtonGroup>
            <Button primary onClick={handleSubmit} type="submit" ><p>Register</p></Button>
            <Button secondary type="button" ><Link to={`/`}>Back</Link></Button>
          </ButtonGroup>
        </FormGroup>
      </WrapperFormLogin>
    )}
  </Formik>
);

export default LoginForm;
