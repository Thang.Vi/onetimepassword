import styled from 'styled-components'
import BgLogin from '../../../images/bgLogin.png';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 100vh;
  width: 100%;
  background: url(${BgLogin}) center center / cover no-repeat scroll rgba(27, 92, 195, 0.8);

`

export default Container