import React, { useEffect } from 'react'
import useRegister from '../../state/auth/hooks/useRegister'
import Container from './containers/container'
import Form from './containers/form'
import Spinner from '../../components/spinner'
import SystemAlert from '../../components/systemAlert'

const Register = (props) => {
  const { from } = props.location.state || {from: {pathname: "/"}};
  const [auth, setRegister, isLoading, err, alerts] = useRegister();


  useEffect(() => {
    if (alerts && alerts.type == 'success') {
      setTimeout(function(){ props.history.push(from); }, 2000);
    }
  }, [alerts]);

  return (
    <Container>
      <Spinner show={isLoading} />
      {alerts && alerts.type ? <SystemAlert alerts={alerts} /> : ''}
      <Form onSubmit={(values, actions) => setRegister({values, actions})} err={err} />
    </Container>
  )
};

export default Register
