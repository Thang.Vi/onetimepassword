import React from 'react'
import {BrowserRouter as Router, Route,Switch} from 'react-router-dom'
import {StateProvider} from '../state';
import { INITIAL_STATE as AUTH_INITIAL_STATE } from '../state/auth/reducers'
import reducers from "../state/reducers";
import BaseStyles from './base-styles';
import PrivateRoute from './private-route'
import Content from '../components/content'
import Login from '../views/login'
import Register from '../views/register'
import Home from '../views/home'

const Root = props => {
    const initialState = {
      auth: AUTH_INITIAL_STATE
    }
    return (
      <StateProvider initialState={initialState} reducer={reducers}>
        <BaseStyles />
        <Router>
          <>
            <Content>
              <Switch>
                <Route exact path="/" component={Login} />
                <Route exact path="/register" component={Register} />
                <PrivateRoute path="/home" component={Home} />
              </Switch>
            </Content>
          </>
        </Router>
      </StateProvider>
    )
};

export default Root
