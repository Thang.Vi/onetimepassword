import React, {useEffect} from 'react'
import {Route, Redirect} from 'react-router-dom'

import { useStateValue } from '../../state'

const PrivateRoute = ({component: Component, location, ...rest}) => {
  const [{auth}] = useStateValue()
  const token = localStorage.getItem('token')
  const account = JSON.parse(localStorage.getItem('account'))
  useEffect(() => {
    if(token && account) {
      auth.logged = true
    } else {
      auth.logged = false
    }
  }, [])

  return (
    <Route {...rest}
     render={props => (
        auth.logged && location.pathname != "/" ?
          <Component {...props} />
         : 
          <Redirect to={{pathname: "/", state: {from: props.location}}}/>
     )}
    />
  )
};

export default PrivateRoute