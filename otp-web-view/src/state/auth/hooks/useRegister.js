import { useState } from 'react'
import { useStateValue } from '../../index'
import { userRegister } from '../queries'
import {
  login, showAlert
} from '../actions'

const useRegister = () => {
  const [{auth}, dispatch] = useStateValue()
  const [isLoading, setIsLoading] = useState(false)
  const [err, setErr] = useState({})
  const [alerts, setAlerts] = useState({})

  const formData = async ({values, actions}) => {
    setIsLoading(true)

    let rex = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/;
    let checkUser = rex.exec(values.username);
    if (checkUser) {
      if (values.password && (values.passwordConfirm == values.password)) {
        let responseData = await userRegister(values)
        switch(responseData) 
          {
            case 'Register Success!':
              setErr({user: '', pass: '', passConfirm: ''});
              setAlerts({ type: 'success', message: responseData, show: true});
              break;
            case 'User has exist!':
              setErr({user: responseData, pass: '', passConfirm: ''});
              setAlerts({ type: 'error', message: responseData, show: true});
              break;
            default:
              setAlerts({ type: 'error', message: responseData, show: true});
              break;
          } 
      }
      if (values.password && (values.passwordConfirm != values.password)) {
        setErr({user: '', pass: '', passConfirm: 'Password confirm Invalid'})
      } 
      if (!values.password && !values.passwordConfirm) {
        setErr({user: '', pass: 'Password Invalid', passConfirm: 'Password confirm Invalid'})
      }
    } else {
      if (!values.password && !values.passwordConfirm) {
        setErr({user: 'Email Invalid', pass: 'Password Invalid', passConfirm: 'Password confirm Invalid'})
      } else {
        setErr({user: 'Email Invalid', pass: '', passConfirm: ''})
      }
      
    }
    setIsLoading(false)
  }

  return [auth, formData, isLoading, err, alerts]
}

export default useRegister
