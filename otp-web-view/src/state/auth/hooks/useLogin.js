import { useState } from 'react'
import { useStateValue } from '../../index'
import { userLogin } from '../queries'
import {
  login
} from '../actions'

const useLogin = () => {
  const [{auth}, dispatch] = useStateValue()
  const [isLoading, setIsLoading] = useState(false)
  const [err, setErr] = useState({})
  const [token, setToken] = useState()

  const formData = async ({values, actions}) => {
    setIsLoading(true)

    let rex = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/;
    let checkUser = rex.exec(values.username);

    if (values.password != '') {
      if (checkUser) {
        let responseData = await userLogin(values)
        if(responseData && responseData != "Account does not exist" && responseData != "Not found!") {
          if(responseData.token && responseData.token.length > 0) {
            setToken(responseData.token)
            localStorage.setItem('token', responseData.token)
          }
          localStorage.setItem('account', JSON.stringify(responseData.account))
        }
        if(responseData && responseData.account) {
          setErr({user: '', pass: '', OTP: ''})
          dispatch(login(responseData))
        }else if(responseData && !responseData.account) {
          switch(responseData) 
          {
            case 'Password Incorrect':
              setErr({user: '', pass: responseData, OTP: ''})
              break;
            case 'OTP Incorrect':
              setErr({user: '', pass: '', OTP: responseData})
              break;
            default:
              setErr({user: responseData, pass: '', OTP: ''})
              break;
          } 
        }
      } else {
        setErr({user: 'Email Invalid', pass: '', OTP: ''})
      }
    } else {
      if (checkUser) {
        setErr({user: '', pass: 'Password Invalid', OTP: ''})
      } else {
        setErr({user: 'Email Invalid', pass: 'Password Invalid', OTP: ''})
      }
    }

    setIsLoading(false)
  }

  return [auth, formData, isLoading, err, token]
}

export default useLogin
