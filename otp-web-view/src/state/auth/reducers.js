import {LOGIN, LOGOUT} from "./actions";

export const INITIAL_STATE = {
  logged: false,
  systemAlert: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case LOGIN:
      return {
        logged: true
      }
      break;
    case LOGOUT:
      return {
        ...INITIAL_STATE
      };
      break;
    default:
      return state;
  }
};