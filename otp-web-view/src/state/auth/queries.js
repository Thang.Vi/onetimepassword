import axios from 'axios'
import api from '../../utils/services'

export const userLogin = (values) => {
	var postData = {
		username: values.username,
		password: values.password,
		otp: values.otp
	};
	return axios.post(
			api.url + `/authen/login`, 
			postData
		)
		.then(res => res.data)
		.catch(err => err.response.data)
};

export const userRegister = (values) => {
	var postData = {
		username: values.username,
		password: values.password
	};
	return axios.post(
			api.url + `/authen/register`, 
			postData
		)
		.then(res => res.data)
		.catch(err => err.response.data)
};